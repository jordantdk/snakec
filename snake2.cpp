#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <cstdio>
#include <termios.h>
#include <time.h>
#include <thread>
using namespace std;

#define UP 65
#define DOWN 66
#define LEFT 68
#define RIGHT 67

typedef struct part{
	struct part *previous;
	int x;
	int y;
	struct part *next;
}part;

typedef struct food{
	int x;
	int y;
}food;

namespace{
	part *head=NULL;
	part *tail=NULL;
	food *apple=NULL;
	bool gameOver=false;
	int *currentKey=NULL;
	int *previousKey=NULL;
	int score=0;
}


void moveSnake();
int getch(void);
void square();
void add();
void adjustChords(int key);
void genFood();
bool spaceEmpty(int x,int y);
void gameStats();

int main(){
	apple=new food;
	int temp,previous,current;
	currentKey=&previous;//assigns address to global pointer
	genFood();
	add();
	add();
	add();
	//moveSnake();
	thread t1(moveSnake);
	thread t2(gameStats);
	t1.detach();
	t2.detach();
	current=RIGHT;
	while(!gameOver){
		previous=current;
		temp=getch();
		if(temp==DOWN && previous!=UP){
			current=DOWN;
		}else if(temp==UP && previous!=DOWN){
			current=UP;
		}else if(temp==LEFT && previous!=RIGHT){
			current=LEFT;
		}else if(temp==RIGHT && previous!=LEFT){
			current=RIGHT;
		}
	}
	cout<<"\t\t\tGAME OVER\n\t\t     FINAL SCORE:"<<score<<endl;
}
void gameStats(){
	part *current;
	while(!gameOver){
		//if head coordinates equal to food coordinates
		if(head->x==apple->x && head->y==apple->y){
			score+=10;
			add();
			genFood();
		}
		for(current=head->next->next;current!=NULL;current=current->next){
			if(head->x==current->x && head->y==current->y){
				gameOver=true;
			}
		}
	}
}
void genFood(){
	int tempx,tempy;
	srand(time(NULL));
	while(true){
		tempx=(rand()%28)+1;
		tempy=(rand()%58)+1;
		if(spaceEmpty(tempx,tempy)){
			break;
		}
		break;
	}
	apple->x=tempx;
	apple->y=tempy;
}
//function to check if space is occupied by snake
bool spaceEmpty(int x,int y){
	part *current;
	//loop through snake nodes
	for(current=head;current!=NULL;current=current->next){
		if(current->x==x && current->y==y){
			return false;
		}
	}
	return true;
}
void square(){
	int x,y;
	bool occupied;//changes to true if a space is occupied by food or snake node
	part *current;
	system("clear");
	cout<<"\t\t$N@KE\n"<<"\t\t\t\tSCORE:"<<score<<endl;
	for(x=0;x<30;x++){
		for(y=0;y<60;y++){
			occupied=false;//resets for each line
			//prints edges of box
			if(x==0 || x==29 || y==0 || y==59){
				cout<<"#";
			}else{//spaces that are not edges
				//print snake node
				for(current=head;current!=NULL;current=current->next){
					if(current->x==x && current->y==y){
						cout<<"@";
						occupied=true;
					}
				}
				//print food
				if(apple->x==x && apple->y==y){
					cout<<"o";
					occupied=true;
				}
				//print space if position not occupied
				if(!occupied){
					cout<<" ";
				}
			}
		}
		cout<<"\n";
	}
}
void add(){
	part *node=new part;
	//if list is empty
	if(head==NULL){
		node->x=15;
		node->y=29;
		head=node;
	}else{
		tail->next=node;
		node->previous=tail;
		if(*currentKey==UP){
			node->x=node->previous->x+1;
			node->y=node->previous->y;
		}else if(*currentKey==DOWN){
			node->x=node->previous->x-1;
			node->y=node->previous->y;
		}else if(*currentKey==LEFT){
			node->x=node->previous->x;
			node->y=node->previous->y+1;
		}else if(*currentKey==RIGHT){
			node->x=node->previous->x;
			node->y=node->previous->y-1;
		}
	}
	tail=node;
}
void adjustChords(int key){
	part *current;
	int previousx,previousy;
	previousx=head->x;
	previousy=head->y;
	if(key==UP){
		head->x-=1;
	}else if(key==DOWN){
		head->x+=1;
	}else if(key==RIGHT){
		head->y+=1;
	}else if(key==LEFT){
		head->y-=1;
	}
	if(head->x==29){
		head->x=1;
	}else if(head->x==0){
		head->x=28;
	}
	if(head->y==59){
		head->y=1;
	}else if(head->y==0){
		head->y=58;
	}
	if(key==UP || key==DOWN || key==LEFT || key==RIGHT){
		for(current=tail;current!=head;current=current->previous){
			if(current==head->next){
				current->x=previousx;
				current->y=previousy;
			}else{
				current->x=current->previous->x;
				current->y=current->previous->y;
			}
		}
	}
}
void moveSnake(){
	while(!gameOver){
		usleep(130000);
		if(*currentKey==UP || *currentKey==DOWN || *currentKey==LEFT || *currentKey==RIGHT){
			adjustChords(*currentKey);
		}
		square();
	}
}
int getch(void)
{
 int ch;
 struct termios oldt;
 struct termios newt;
 tcgetattr(STDIN_FILENO, &oldt); /*store old settings */
 newt = oldt; /* copy old settings to new settings */
 newt.c_lflag &= ~(ICANON | ECHO); /* make one change to old settings in new settings */
 tcsetattr(STDIN_FILENO, TCSANOW, &newt); /*apply the new settings immediatly */
 ch = getchar(); /* standard getchar call */
 tcsetattr(STDIN_FILENO, TCSANOW, &oldt); /*reapply the old settings */
 return ch; /*return received char */
}